﻿using System;  
using System.DirectoryServices.AccountManagement;  
using System.Security;

namespace ad-expirepw  
{  
    class expirepw  
    {  
        
        static void Main(string[] args)  
        {  
            try  
            {  
                // enter AD settings
                Console.Write("Enter domain: ")
                string DOMAIN = Convert.ToString(Console.ReadLine());
                Console.Write("Enter username: ");
                string adminUsername = Convert.ToString(Console.ReadLine());
                Console.Write("Eenter Password: ");
                string adminPassword = "";
                while (true)
                {
                    var key = System.Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Enter)
                        break;
                    adminPassword += Convert.ToString(key.KeyChar);
                }
                // AD Login
                PrincipalContext AD = new PrincipalContext(ContextType.Domain, DOMAIN, adminUsername, adminPassword);   
                // reading from a txt file
                string path = "";
                string[] lines = System.IO.File.ReadAllLines(path);
                List<string> ErrorUsers = new List<string>();
                foreach(string User in lines)
                {
                    try
                    {
                        // create search user and add criteria
                        UserPrincipal user = new UserPrincipal(AD);      
                        user.SamAccountName = User;  
                        // search for user  
                        PrincipalSearcher search = new PrincipalSearcher(user);  
                        UserPrincipal userresult = (UserPrincipal)search.FindOne();  
                        search.Dispose();
                        Console.WriteLine("\n"+"Resetting password for: "+userresult.DisplayName);
                        // expires password
                        userresult.ExpirePasswordNow();
                        userresult.Save();  
                    }
                    catch (Exception e)  
                    {  
                        ErrorUsers.Add(User);
                        Console.WriteLine("\nError on " + User +": " + e.Message);  
                    }  
                }
                Console.WriteLine("\nERROR USERS");
                foreach (string ErrorUser in ErrorUsers)
                {
                    Console.WriteLine(ErrorUser);
                }
            }  
            catch (Exception e)  
            {  
                Console.WriteLine("Error: " + e.Message);  
            }  
        }  
    }  
}  